#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>

#include <audio.h>
#include <speechtotext.h>

class SpeechToText;

class Engine : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString tokenPath READ tokenPath WRITE setTokenPath NOTIFY tokenPathChanged)
    Q_PROPERTY(QString languageCode READ languageCode WRITE setLanguageCode NOTIFY languageCodeChanged)

public:
    explicit Engine(QObject *parent = nullptr);
    virtual ~Engine();

public slots:

    // you must call init() before start.
    bool init();

    void start();

    void play();

    QString printToken();

    QString tokenPath() const;
    void setTokenPath(const QString &path);
    QString languageCode() const;
    void setLanguageCode(const QString &code);

    void onResult(const QString &text);

private:
    speech::audio::Audio audio;
    speech::google::SpeechToText stt;

    bool record;

    QString m_tokenPath;
    QString m_languageCode;

    RecognizeConfig config;

signals:
    void tokenPathChanged();
    void languageCodeChanged();
    void recordStarted();
    void recordEnd();

};

#endif // ENGINE_H
