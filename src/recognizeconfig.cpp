#include <recognizeconfig.h>

namespace speech {
namespace config {

RecognizeConfig::RecognizeConfig()
    : m_encoding("")
    , m_languageCode("")
    , m_sampleRateHertz(-1)
{

}

RecognizeConfig::~RecognizeConfig()
{

}

RecognizeConfig &RecognizeConfig::operator=(const RecognizeConfig &object)
{
    this->m_encoding = object.m_encoding;
    this->m_languageCode = object.m_languageCode;
    this->m_sampleRateHertz = object.m_sampleRateHertz;

    return *this;
}

bool RecognizeConfig::operator==(const RecognizeConfig &object) const
{
    if (object.m_encoding == m_encoding &&
            object.m_languageCode == m_languageCode &&
            object.m_sampleRateHertz == m_sampleRateHertz) {
        return true;
    } else {
        return false;
    }
}

bool RecognizeConfig::isValid() const
{
    return !m_encoding.isEmpty() && !m_languageCode.isEmpty() && !(m_sampleRateHertz == -1);
}

QString RecognizeConfig::encoding() const
{
    return m_encoding;
}

void RecognizeConfig::setEncoding(const Encoding &encoding)
{
    if(encoding == Encoding::LINEAR16)
        m_encoding = "LINEAR16";
    else if(encoding == Encoding::FLAC)
        m_encoding = "FLAC";
}

QString RecognizeConfig::languageCode() const
{
    return m_languageCode;
}

void RecognizeConfig::setLanguageCode(const QString &languageCode)
{
    m_languageCode = languageCode;
}

int RecognizeConfig::sampleRateHertz() const
{
    return m_sampleRateHertz;
}

void RecognizeConfig::setSampleRateHartz(SampleHertz hertz)
{
    if(hertz < SampleHertz::_8000Hz || hertz > SampleHertz::MAX)
        return;

    m_sampleRateHertz = static_cast<int>(hertz);
}

} // namespace config
} // namespace speech

