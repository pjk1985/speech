#include "recognizerequest.h"

namespace speech {
namespace request {

RecognizeRequest::RecognizeRequest()
{
    m_audio.fill(0);
}

void RecognizeRequest::setAudio(const QByteArray &audio)
{
    if(!audio.isEmpty())
        m_audio.replace(0, audio.length(), audio);
}

const QByteArray &RecognizeRequest::audio() const
{
    return m_audio;
}

void RecognizeRequest::setConfig(const RecognizeConfig &config)
{
    m_config = config;
}

const RecognizeConfig &RecognizeRequest::config() const
{
    return m_config;
}

} // namespace request
} // namespace speech

