#include <QAudioInput>
#include <QAudioOutput>

#include <QCoreApplication>

#include <audio.h>
#include <utils.h>

namespace speech {
namespace audio {

const qint64 BufferDurationUs       = 20 * 100000; //1000000

Audio::Audio(QObject *parent)
    : QObject(parent)
    , m_mode(QAudio::AudioInput)
    , m_state(QAudio::StoppedState)
    , m_audioInputDevice(QAudioDeviceInfo::defaultInputDevice())
    , m_audioOutputDevice(QAudioDeviceInfo::defaultOutputDevice())
    , m_audioInput(nullptr)
    , m_audioInputIODevice(nullptr)
    , m_bufferPosition(0)
    , m_bufferLength(0)
    , m_dataLength(0)
    , m_recordPosition(0)
{

}

Audio::~Audio()
{
    stopListening();
    delete m_audioInput;
    m_audioInput = nullptr;
    m_audioInputIODevice = nullptr;
    setRecordPosition(0);
}

void Audio::initialize()
{
    ENGINE_DEBUG << Q_FUNC_INFO;

    selectFormat();

    m_bufferLength = audioLength(m_format, BufferDurationUs);
    m_audioInput = new QAudioInput(m_audioInputDevice, m_format, this);
    m_audioInput->setNotifyInterval(100);

    m_audioOutput = new QAudioOutput(m_audioOutputDevice, m_format, this);
    m_audioOutput->setNotifyInterval(100);

    ENGINE_DEBUG << "Engine::initialize" << "m_bufferLength" << m_bufferLength;
    ENGINE_DEBUG << "Engine::initialize" << "m_dataLength" << m_dataLength;
    ENGINE_DEBUG << "Engine::initialize" << "format" << m_format;
}

void Audio::startListening()
{
    ENGINE_DEBUG << Q_FUNC_INFO;

//    m_bufferLength = audioLength(m_format, BufferDurationUs);
    m_buffer.resize(m_bufferLength);
    m_buffer.fill(0);

    m_mode = QAudio::AudioInput;
    m_dataLength = 0;
    stopPlayback();

    connect(m_audioInput, SIGNAL(stateChanged(QAudio::State)),
            this, SLOT(audioStateChanged(QAudio::State)));

    connect(m_audioInput, SIGNAL(notify()),
            this, SLOT(audioNotify()));

    m_audioInputIODevice = m_audioInput->start();

    connect(m_audioInputIODevice, SIGNAL(readyRead()),
            this, SLOT(audioDataReady()));
}

void Audio::audioDataReady()
{
    ENGINE_DEBUG << Q_FUNC_INFO;

    const qint64 bytesReady = m_audioInput->bytesReady();
    const qint64 bytesSpace = m_buffer.size() - m_dataLength;
    const qint64 bytesToRead = qMin(bytesReady, bytesSpace);

    const qint64 bytesRead = m_audioInputIODevice->read(
                                       m_buffer.data() + m_dataLength,
                                       bytesToRead);

    if (bytesRead) {
        m_dataLength += bytesRead;
        emit dataLengthChanged(m_dataLength);
    }

    ENGINE_DEBUG << Q_FUNC_INFO << "size: " << m_dataLength;

    if (m_buffer.size() == m_dataLength)
        stopListening();
}

void Audio::stopListening()
{
    ENGINE_DEBUG << Q_FUNC_INFO;

    if (m_audioInput) {
        m_audioInput->stop();
        QCoreApplication::instance()->processEvents();
        m_audioInput->disconnect();
    }

    if(m_audioInputIODevice){
        m_audioInputIODevice->disconnect();
        m_audioInputIODevice = nullptr;
    }
}

void Audio::suspend()
{
    ENGINE_DEBUG << Q_FUNC_INFO;

    if (QAudio::ActiveState == m_state ||
        QAudio::IdleState == m_state) {
        switch (m_mode) {
        case QAudio::AudioInput:
            m_audioInput->suspend();
            break;
        case QAudio::AudioOutput:
//            m_audioOutput->suspend();
            break;
        }
    }
}

QAudio::State Audio::state() const
{
    return m_state;
}

const QByteArray &Audio::getData() const
{
    return m_buffer;
}

void Audio::audioStateChanged(QAudio::State state)
{
    ENGINE_DEBUG << "Engine::audioStateChanged from" << m_state
                 << "to" << state;

    if (QAudio::StoppedState == state) {
        // Check error
        QAudio::Error error = QAudio::NoError;
        switch (m_mode) {
        case QAudio::AudioInput:
            error = m_audioInput->error();
            break;
        case QAudio::AudioOutput:
//            error = m_audioOutput->error();
            break;
        }
        if (QAudio::NoError != error) {
//            reset();
            return;
        }
    }

    setState(state);
}

void Audio::audioNotify()
{
    if(m_mode == QAudio::AudioInput){
        const qint64 recordPosition = qMin(m_bufferLength, audioLength(m_format, m_audioInput->processedUSecs()));
        setRecordPosition(recordPosition);

        emit bufferChanged(0, m_dataLength, m_buffer);
    }
    else if(m_mode == QAudio::AudioOutput){
        ENGINE_DEBUG << Q_FUNC_INFO;
        const qint64 playPosition = audioLength(m_format, m_audioOutput->processedUSecs());
        setPlayPosition(qMin(m_bufferLength, playPosition));

        if (playPosition >= m_dataLength)
            stopPlayback();
    }
}

void Audio::setRecordPosition(qint64 position, bool forceEmit)
{
    const bool changed = (m_recordPosition != position);
    m_recordPosition = position;
    if (changed || forceEmit)
        emit recordPositionChanged(m_recordPosition);
}

void Audio::selectFormat(){

    bool foundSupportedFormat = false;

    QList<int> sampleRatesList;
#ifdef Q_OS_WIN
    // The Windows audio backend does not correctly report format support
    // (see QTBUG-9100).  Furthermore, although the audio subsystem captures
    // at 11025Hz, the resulting audio is corrupted.
    sampleRatesList += 8000;
#endif

    if (!m_generateTone)
        sampleRatesList += m_audioInputDevice.supportedSampleRates();

    sampleRatesList = sampleRatesList.toSet().toList(); // remove duplicates
    std::sort(sampleRatesList.begin(), sampleRatesList.end());

    ENGINE_DEBUG << "Engine::initialize frequenciesList" << sampleRatesList;

    QList<int> channelsList;
    channelsList += m_audioInputDevice.supportedChannelCounts();
    channelsList = channelsList.toSet().toList();
    std::sort(channelsList.begin(), channelsList.end());

    ENGINE_DEBUG << "Engine::initialize channelsList" << channelsList;

    QAudioFormat format;
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setCodec("audio/pcm");
    format.setSampleSize(16);
    format.setSampleType(QAudioFormat::SignedInt);
    int sampleRate, channels;
    foreach (sampleRate, sampleRatesList) {
        if (foundSupportedFormat)
            break;
        format.setSampleRate(sampleRate);
        foreach (channels, channelsList) {
            format.setChannelCount(channels);
            const bool inputSupport = m_generateTone ||
                                      m_audioInputDevice.isFormatSupported(format);
            ENGINE_DEBUG << "Engine::initialize checking " << format
                         << "input" << inputSupport;
            if (inputSupport) {
                foundSupportedFormat = true;
                break;
            }
        }
    }

    if (!foundSupportedFormat)
        format = QAudioFormat();

    setFormat(format);
}

void Audio::setFormat(const QAudioFormat &format)
{
    const bool changed = (format != m_format);
    m_format = format;

    if (changed)
        emit formatChanged(m_format);
}

void Audio::setState(QAudio::State state)
{
    const bool changed = (m_state != state);
    m_state = state;
    if (changed)
        emit stateChanged(m_mode, m_state);
}

void Audio::setPlayPosition(qint64 position, bool forceEmit)
{
    const bool changed = (m_playPosition != position);
    m_playPosition = position;
    if (changed || forceEmit)
        emit playPositionChanged(m_playPosition);
}

void Audio::stopPlayback()
{
    if (m_audioOutput) {
        m_audioOutput->stop();
        QCoreApplication::instance()->processEvents();
        m_audioOutput->disconnect();
        setPlayPosition(0);
    }
}

void Audio::startPlayback()
{
    if (m_audioOutput) {
        if (QAudio::AudioOutput == m_mode &&
            QAudio::SuspendedState == m_state) {
#ifdef Q_OS_WIN
            // The Windows backend seems to internally go back into ActiveState
            // while still returning SuspendedState, so to ensure that it doesn't
            // ignore the resume() call, we first re-suspend
            m_audioOutput->suspend();
#endif
            m_audioOutput->resume();
        } else {
            setPlayPosition(0, true);
            stopListening();
            m_mode = QAudio::AudioOutput;
            CHECKED_CONNECT(m_audioOutput, SIGNAL(stateChanged(QAudio::State)),
                            this, SLOT(audioStateChanged(QAudio::State)));
            CHECKED_CONNECT(m_audioOutput, SIGNAL(notify()),
                            this, SLOT(audioNotify()));

            m_audioOutputIODevice.close();
            m_audioOutputIODevice.setBuffer(&m_buffer);
            m_audioOutputIODevice.open(QIODevice::ReadOnly);
            m_audioOutput->start(&m_audioOutputIODevice);
        }
    }
}

} // namespace audio
} // namespace speech

