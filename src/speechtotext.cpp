#include <speechtotext.h>

#include <QNetworkAccessManager>
#include <QtNetwork>
#include <QNetworkReply>
#include <QUrl>

#include <QDataStream>

#include <QJsonDocument>

namespace speech {
namespace google{

const QString method_longrecognize("/v1/speech:longrunningrecognize");
const QString method_recognize("/v1/speech:recognize");

class SpeechToText::SpeechToTextImpl
{
public:
    SpeechToTextImpl()
        : m_tokenPath("")
        , m_token("")
        , m_key("")
        , m_reply(Q_NULLPTR)
    {

    }

    ~SpeechToTextImpl()
    {
        m_reply = Q_NULLPTR;
    }

    bool getToken()
    {
        qDebug() << m_tokenPath;
        QFile file(m_tokenPath);

        if(file.open(QFile::ReadOnly)){
            char buf[1024];
            while(!file.atEnd()){
                qint64 lineLength = file.readLine(buf, sizeof(buf));
                if (lineLength != -1) {
                    // the line is available in buf
                    qDebug() << buf;
                    m_token.append(buf);
                }
            }

            qDebug() << Q_FUNC_INFO << m_token;

            if(!m_token.isEmpty()){
                return true;
            }
        }
        qDebug() << Q_FUNC_INFO << QString("Read failed.");
        return false;
    }

    void getJsonFormat(const speech::request::RecognizeRequest &req, QByteArray &json)
    {
        if(req.audio().isEmpty())
            return;

        QJsonObject rootObj;

        QJsonObject config;
        config["encoding"]              = req.config().encoding();
        config["sampleRateHertz"]       = req.config().sampleRateHertz();
        config["languageCode"]          = req.config().languageCode();
        config["enableWordTimeOffsets"] = false;

        QJsonObject audio;
        audio["content"]                = QString(req.audio());

        rootObj["config"]               = config;
        rootObj["audio"]                = audio;

        QJsonDocument doc(rootObj);

        json = doc.toJson(QJsonDocument::Compact);
    }

    QString getTranscript(const QByteArray &json)
    {
        if(json.isEmpty())
            return "";

        auto doc = QJsonDocument::fromJson(json);
        auto rootObj = doc.object();

        auto resultsValue = rootObj.value("results");
        auto resultArray = resultsValue.toArray();

        for(auto item: resultArray){
            auto alternativesValue = item.toObject().value("alternatives");
            auto alternativesArray = alternativesValue.toArray();

            for(auto elment : alternativesArray){
                auto transcript = elment.toObject().value("transcript");
                if(!transcript.toString().isEmpty())
                    return transcript.toString();
            }
        }

        return "";
    }

    QString m_tokenPath;
    QByteArray m_token;
    QString m_key;
    QUrl m_url;
    QNetworkAccessManager m_networkmanager;
    QNetworkReply *m_reply;
};

SpeechToText::SpeechToText(QObject *parent)
    : QObject (parent)
    , pImpl(QSharedPointer<SpeechToTextImpl>(new SpeechToTextImpl()))
{
    this->setUrl();
}

SpeechToText::~SpeechToText()
{

}

void SpeechToText::setUrl(const QString &url)
{
    pImpl->m_url = QUrl::fromUserInput(url);

    if(!pImpl->m_url.isValid()){
        qDebug() << QString("Invalid URL: %1").arg(pImpl->m_url.errorString());
        return;
    }
}

bool SpeechToText::setTokenPath(const QString &path)
{
    if(path.isEmpty())
        return false;

    pImpl->m_tokenPath = path;

    return pImpl->getToken();
}

bool SpeechToText::setAPIKey(const QString &key)
{
    if(key.isEmpty())
        return false;

    pImpl->m_key = key;

    return true;
}

void SpeechToText::recognize(const request::RecognizeRequest &req)
{
    if(req.audio().isEmpty() || !req.config().isValid()){
        qDebug() << Q_FUNC_INFO << QString("Error Config");
        return;
    }

    QByteArray data;
    pImpl->getJsonFormat(req, data);

    if(pImpl->m_url.isEmpty() || (pImpl->m_token.isEmpty() && pImpl->m_key.isEmpty())){
        qDebug() << Q_FUNC_INFO << QString("Error");
        return;
    }

    QUrl uri(pImpl->m_url.toString() + method_recognize + (pImpl->m_key.isEmpty() ? "" : "?key=" + pImpl->m_key));
    QNetworkRequest request(uri);
    request.setRawHeader("Content-Type", "application/json");

    if(pImpl->m_key.isEmpty())
        request.setRawHeader("Authorization", "Bearer " + pImpl->m_token);

    pImpl->m_reply = pImpl->m_networkmanager.post(request, data);

    connect(pImpl->m_reply, static_cast<void (QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), [&](QNetworkReply::NetworkError error){
        if(error != QNetworkReply::NetworkError::NoError){
            qDebug() << Q_FUNC_INFO << "Http Request Error :" << pImpl->m_reply->errorString();
            pImpl->m_reply->deleteLater();
            pImpl->m_reply = Q_NULLPTR;
        }
    });

    connect(pImpl->m_reply, &QNetworkReply::finished, [&](){
        qDebug() << Q_FUNC_INFO ;
        pImpl->m_reply->deleteLater();
        pImpl->m_reply = Q_NULLPTR;
    });

    connect(pImpl->m_reply, &QNetworkReply::readyRead, [&](){
        auto result = pImpl->m_reply->readAll();
        qDebug() << Q_FUNC_INFO << result;
        if(!result.isEmpty()){
            auto text = pImpl->getTranscript(result);
            this->result(text);
        }
    });
}

void SpeechToText::longrunningrecognize(const speech::request::RecognizeRequest &req)
{
    Q_UNUSED(req)

    QUrl newUrl(pImpl->m_url.toString() + method_longrecognize);

    //TODO:
}

} // namespace google
} // namespace speech
