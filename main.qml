import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2
import SpeechEngine 1.0

Window {
    id: idWindow
    visible: true
    width: 1000
    height: 600
    title: qsTr("Google Speech Test")

    Rectangle {
        id: viewer
        color: "black"
        anchors.fill: parent

        Item{
            anchors.fill: parent
            Column {
                id: buttons
                anchors.left: parent.left
                width: parent.width/4
                height: parent.height

                Button{
                    width: parent.width
                    text: qsTr("Open Token file")
                    onClicked: {
                        fileDialog.visible = true
                    }
                }

                Button{
                    width: parent.width
                    text: qsTr("Verify Key")
                    onClicked: {
                        logView.append(speech.printToken())
                    }
                }

                Button {
                    width: parent.width
                    enabled: !speech.isListening
                    text: speech.isListening? qsTr("Listening") : qsTr("Start Voice Reconize")
                    onClicked: {
                        if(!speech.isInitialize){
                            if(speech.init())
                                speech.isInitialize = true;
                        }

                        if(speech.isInitialize){
                            speech.start()
                        }else
                            logView.append("Engine Dose not initialize.")
                    }
                }

                Button {
                    width: parent.width
                    enabled: !speech.isListening
                    text: qsTr("Play")
                    onClicked: {
                        if(!speech.isInitialize){
                            if(speech.init())
                                speech.isInitialize = true;
                        }

                        if(speech.isInitialize){
                            speech.play()
                            logView.append("play")
                        }else
                            logView.append("Engine Dose not initialize.")
                    }
                }


                Button {
                    width: parent.width
                    text: speech.isKor? qsTr("한국어") : qsTr("English")
                    onClicked: {
                        speech.isKor = !speech.isKor
                        logView.append("language: " +  text)
                    }
                }

                Button{
                    width: parent.width
                    text: qsTr("Exit")
                    onClicked: {
                        Qt.quit()
                    }
                }
            }

            Rectangle{
                anchors.left: buttons.right
                anchors.right: parent.right
                height: parent.height

                border.width: 1
                border.color: "black"

                TextArea {
                    objectName: "logView"
                    id: logView
                    anchors.fill: parent
                    wrapMode: TextEdit.Wrap
                    onTextChanged: {
                        if(logView.length > 150000) {
                            logView.remove(0, 10000);
                        }
                        cursorPosition = text.length
                    }
                }
            }
        }
    }

    FileDialog {
        id: fileDialog
        nameFilters: [ "Token files (*.txt)", "All files (*)" ]
        title: "Please choose a file"

        onAccepted: {
            logView.append("You chose: " + fileDialog.fileUrl)
            speech.setTokenPath(fileDialog.fileUrl)
            visible = false
        }
        onRejected: {
            logView.append("Canceled")
            visible = false
        }
    }

    SpeechEngine{
        id: speech
        property bool isInitialize: false
        property bool isKor: true
        property bool isListening: false

        languageCode: isKor? "ko-KR" : "en-US"

        onRecordStarted: {
            logView.append("Record Started")
            isListening = !isListening
        }

        onRecordEnd: {
            logView.append("Record End")
            isListening = !isListening
        }
    }
}
