#ifndef TYPES_H
#define TYPES_H

namespace speech {
namespace types {

enum class Encoding{
    LINEAR16,
    FLAC,
};

enum class LanguageCode{
    KO_KOR,
    EN_US,
};

enum class SampleHertz{
    _8000Hz = 8000,
    _11025Hz = 11025,
    _16000Hz = 16000,
    _22050Hz = 22050,
    MAX = _22050Hz
};


} // namespace types
} // namespace speech


#endif // TYPES_H
