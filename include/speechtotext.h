#ifndef SPEECHTOTEXT_H
#define SPEECHTOTEXT_H

#include <QObject>
#include <QSharedPointer>

#include <recognizerequest.h>

namespace speech {
namespace google{

class SpeechToText : public QObject
{
    Q_OBJECT
public:
    explicit SpeechToText(QObject *parent = nullptr);
    virtual ~SpeechToText();

    /*!
     * \brief recognize
     * Performs synchronous speech recognition.
     * \param req
     */
    void recognize(const speech::request::RecognizeRequest &req);

    /*!
     * \brief longrunningrecognize
     * Performs asynchronous speech recognition.
     */
    void longrunningrecognize(const speech::request::RecognizeRequest &req);

    /*!
     * \brief setUrl
     * for Working with wire protocols (HTTP)
     * \param url
     */
    void setUrl(const QString &url = QString("https://speech.googleapis.com"));

    /*!
     * \brief setTokenPath
     * \param path
     */
    bool setTokenPath(const QString &path);

    /*!
     * \brief setAPIKey
     * \param key
     * you need API KEY so visit https://console.cloud.google.com/apis/credentials
     * \return
     */
    bool setAPIKey(const QString &key);

Q_SIGNALS:
    void result(QString);

private:
    SpeechToText(const SpeechToText&) = delete;
    SpeechToText &operator = (const SpeechToText &) = delete;

private:
    class SpeechToTextImpl;
    QSharedPointer<SpeechToTextImpl> pImpl;
};

} // namespace google
} // namespace speech

#endif // SPEECHTOTEXT_H
