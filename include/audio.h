#ifndef AUDIO_H
#define AUDIO_H

#include <QAudioDeviceInfo>
#include <QBuffer>

#define CHECKED_CONNECT(source, signal, receiver, slot) \
    if (!connect(source, signal, receiver, slot)) \
        qt_assert_x(Q_FUNC_INFO, "CHECKED_CONNECT failed", __FILE__, __LINE__);

class QIODevice;
class QAudioInput;
class QAudioOutput;

namespace speech {
namespace audio {

class Audio: public QObject
{
    Q_OBJECT
public:
    explicit Audio(QObject *parent = nullptr);
    virtual ~Audio();
    void initialize();

    void startListening();
    void stopListening();
    void suspend();
    QAudio::State state() const;
    const QByteArray &getData() const;
    void startPlayback();
    void stopPlayback();

private:
    void setRecordPosition(qint64 position, bool forceEmit = false);
    void selectFormat();
    void setFormat(const QAudioFormat &format);
    void setState(QAudio::State state);
    void setPlayPosition(qint64 position, bool forceEmit = false);

private slots:
    void audioDataReady();
    void audioStateChanged(QAudio::State);
    void audioNotify();

signals:
    void dataLengthChanged(qint64 duration);
    void bufferChanged(qint64 position, qint64 length, const QByteArray &buffer);

    void recordPositionChanged(qint64 position);

    void formatChanged(const QAudioFormat &format);
    void stateChanged(QAudio::Mode mode, QAudio::State state);

    void playPositionChanged(qint64 position);

private:
    QAudio::Mode        m_mode;
    QAudio::State       m_state;

    QAudioDeviceInfo    m_audioInputDevice;
    QAudioDeviceInfo    m_audioOutputDevice;
    QAudioInput*        m_audioInput;
    QIODevice*          m_audioInputIODevice;
    QAudioOutput*       m_audioOutput;
    qint64              m_playPosition;
    QBuffer             m_audioOutputIODevice;

    QAudioFormat        m_format;

    QByteArray          m_buffer;
    qint64              m_bufferPosition;
    qint64              m_bufferLength;
    qint64              m_dataLength;

    qint64              m_recordPosition;

    bool                m_generateTone;
};

} // namespace audio
} // namespace speech

#endif // AUDIO_H
