#ifndef RECOGNIZEREQUEST_H
#define RECOGNIZEREQUEST_H

#include <recognizeconfig.h>

using namespace speech::config;

namespace speech {
namespace request {

class RecognizeRequest
{
public:
    RecognizeRequest();

    void setAudio(const QByteArray &audio);

    const QByteArray &audio() const;

    void setConfig(const RecognizeConfig &config);

    const RecognizeConfig &config() const;

private:
    QByteArray m_audio;
    RecognizeConfig m_config;

};

} // namespace request
} // namespace speech


#endif // RECOGNIZEREQUEST_H
