#ifndef RECOGNIZECONFIG_H
#define RECOGNIZECONFIG_H

#include <QString>
#include <types.h>

using namespace speech::types;

namespace speech {
namespace config {

class RecognizeConfig
{
public:

    RecognizeConfig();
    virtual ~RecognizeConfig();

    RecognizeConfig &operator=(const RecognizeConfig& object);
    bool operator==(const RecognizeConfig& object) const;

    bool isValid() const;

    QString encoding() const;

    /*!
     * \brief setEncoding
     * For best results, the audio source should be captured and transmitted using a lossless encoding (FLAC or LINEAR16).
     * \param encoding
     */
    void setEncoding(const Encoding &encoding);

    QString languageCode() const;

    /*!
     * \brief setLanguageCode
     * See http://g.co/cloud/speech/docs/languages for a list of supported languages so a BCP-47 language tag.
     * \param languageCode
     */
    void setLanguageCode(const QString &languageCode);

    int sampleRateHertz() const;

    /*!
     * \brief setSampleRateHartz
     * Valid values are: 8000-48000. 16000 is optimal. For best results, set the sampling rate of the audio source to 16000 Hz.
     * \param hertz
     */
    void setSampleRateHartz(SampleHertz hertz);

private:
    QString m_encoding;
    QString m_languageCode;
    int m_sampleRateHertz;

};

} // namespace config
} // namespace speech

#endif // RECOGNIZECONFIG_H
