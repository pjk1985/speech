/// QML Type Registration Helper
#include <QtQml>
#include "engine.h"

void registerQuickSpeechQmlTypes()
{
     qmlRegisterType<Engine>("SpeechEngine", 1, 0, "SpeechEngine");
}

// Allow to disable QML types auto registration as required by #9
#ifndef QUICK_FLUX_DISABLE_AUTO_QML_REGISTER
Q_COREAPP_STARTUP_FUNCTION(registerQuickSpeechQmlTypes)
#endif
