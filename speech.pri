DEFINES += LOG_ENGINE

QT += network multimedia

SOURCES += \
    src/utils.cpp \
    src/audio.cpp \
    src/speechtotext.cpp \
    src/recognizeconfig.cpp \
    src/recognizerequest.cpp

HEADERS += \
    include/utils.h \
    include/audio.h \
    include/speechtotext.h \
    include/recognizeconfig.h \
    include/recognizerequest.h \
    include/types.h

INCLUDEPATH += \
    include/

