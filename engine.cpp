#include <QCoreApplication>
#include <QDebug>
#include <QUrl>
#include <QFile>

#include <recognizerequest.h>
#include "engine.h"

using RecognizeConfig  = speech::config::RecognizeConfig;
using RecognizeRequest = speech::request::RecognizeRequest;

Engine::Engine(QObject *parent)
    : QObject(parent)
    , record(false)
{
    connect(&stt, SIGNAL(result(QString)), this, SLOT(onResult(const QString &)));
}

Engine::~Engine()
{

}

bool Engine::init()
{
    qDebug() << Q_FUNC_INFO;

    config.setEncoding(Encoding::LINEAR16);
    config.setSampleRateHartz(SampleHertz::_8000Hz);

    auto key = printToken();

    if(key.isEmpty()){
        qWarning() << "Does not exits key";
        return false;
    }else{
        audio.initialize();
        stt.setAPIKey(key);
        return true;
    }
}

void Engine::start()
{
    if(audio.state() != QAudio::State::StoppedState)
        audio.stopListening();

    audio.startListening();

    connect(&audio, &speech::audio::Audio::stateChanged, [=](QAudio::Mode mode, QAudio::State state){
        if(mode == QAudio::Mode::AudioInput){
            if(state == QAudio::State::StoppedState){
                auto cpconfig = config;
                cpconfig.setLanguageCode(languageCode());

                RecognizeRequest request;
                request.setConfig(cpconfig);
                request.setAudio(audio.getData().toBase64());

                stt.recognize(request);

                audio.disconnect();

                emit this->recordEnd();
            }
            else if(state == QAudio::State::ActiveState){
                emit this->recordStarted();
            }
        }
    });
}

void Engine::play()
{
    audio.startPlayback();
}

QString Engine::tokenPath() const
{
    return m_tokenPath;
}

void Engine::setTokenPath(const QString &path)
{
    if(path != m_tokenPath){
        m_tokenPath = path;
        emit tokenPathChanged();
    }
}

QString Engine::languageCode() const
{
    return m_languageCode;
}

void Engine::setLanguageCode(const QString &code)
{
    if(code != m_languageCode){
        m_languageCode = code;
        emit languageCodeChanged();
    }
}

void Engine::onResult(const QString &text)
{
    qDebug() << Q_FUNC_INFO << text;

    // TODO: parsing text

}

QString Engine::printToken()
{
    QString key;
    if(m_tokenPath.isEmpty()) {
        qWarning() << "Does not set token file path.";
        return key;
    }

    QUrl url(m_tokenPath);
    QFile file(url.toLocalFile());

    if(!file.exists()) {
        qWarning() << "Does not exits: " << url.toLocalFile();
        return key;
    }

    if(file.open(QIODevice::ReadOnly)) {
        char buf[1024];
        while(!file.atEnd()){
            qint64 lineLength = file.readLine(buf, sizeof(buf));
            if (lineLength != -1) {
                // the line is available in buf
                qDebug() << buf;
                key = buf;
            }
        }
    }
    return key;
}
